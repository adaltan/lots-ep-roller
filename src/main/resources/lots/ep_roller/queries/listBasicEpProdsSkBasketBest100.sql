SELECT A.PROD_ID AS PROD_ID 
	, B.PROD_NM 
	, A.RANK 
	, NVL((SELECT MANUF_NM 
				FROM PC_MANUF_M 
                WHERE MANUF_CD = C.MANUF_CD), '') AS MANUF_NM 
    , NVL((SELECT BRAND_NM 
             FROM PC_BRAND_M 
           WHERE BRAND_CD = C.BRAND_CD 
              AND MANUF_CD = C.MANUF_CD), '') AS BRAND_NM 
    , 'http://www.lotsshop.com/item/itemInfoD.do?' || 
      'organ_cd=' || A.ORGAN_CD || 
      CHR(38) || 'prod_id=' || A.PROD_ID || 
      CHR(38) || 'shop_id=' || A.SHOP_ID || 
      CHR(38) || 'partnership=1432001' AS PROD_URL 
     , 'http://file.lotsshop.com/Attach/prod/prod/prod1st_' || 
       D.IMG_1ST  AS IMG_1ST 
     , FN_COUPON_MAX_AMT(A.SHOP_ID, A.ORGAN_CD, A.PROD_ID, '') AS EVENT_PRICE 
  FROM (SELECT DISTINCT PROD_ID AS PROD_ID 
               , WEEK_SEQ 
               , ORGAN_CD 
               , SHOP_ID 
               , RANK 
               , ROW_NUMBER() OVER (PARTITION BY PROD_ID ORDER BY RANK ASC, UPD_DT DESC) AS RNUM 
          FROM BM_BEST_PROD_T 
         WHERE WEEK_SEQ = (SELECT MAX(WEEK_SEQ) 
                             FROM BM_BEST_PROD_T) 
           AND RANK <= 100) A 
       , PM_PROD_PRIM_T B 
       , PM_PROD_D C 
       , PM_PROD_EXPLN_T D 
 WHERE A.PROD_ID = B.PROD_ID 
   AND A.PROD_ID = C.PROD_ID 
   AND A.ORGAN_CD = C.ORGAN_CD 
   AND C.PROD_ID = D.PROD_ID 
   AND C.ORGAN_CD = D.ORGAN_CD 
   AND A.RNUM = 1 
 ORDER BY A.RANK ASC 	