SELECT COUNT(*)
  FROM (SELECT DISTINCT PROD_ID AS PROD_ID 
               , WEEK_SEQ 
               , ORGAN_CD 
               , SHOP_ID 
               , RANK 
               , ROW_NUMBER() OVER (PARTITION BY PROD_ID ORDER BY RANK ASC, UPD_DT DESC) AS RNUM 
          FROM BM_BEST_PROD_T 
         WHERE WEEK_SEQ = (SELECT MAX(WEEK_SEQ) 
                             FROM BM_BEST_PROD_T) 
           AND RANK <= 100) A 
       , PM_PROD_PRIM_T B 
       , PM_PROD_D C 
       , PM_PROD_EXPLN_T D 
 WHERE A.PROD_ID = B.PROD_ID 
   AND A.PROD_ID = C.PROD_ID 
   AND A.ORGAN_CD = C.ORGAN_CD 
   AND C.PROD_ID = D.PROD_ID 
   AND C.ORGAN_CD = D.ORGAN_CD 
   AND A.RNUM = 1 
 ORDER BY A.RANK ASC 	