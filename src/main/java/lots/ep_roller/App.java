package lots.ep_roller;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import lots.ep_roller.config.AppConfig;
import lots.ep_roller.injections.Guicer;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;
import org.perf4j.StopWatch;
import org.perf4j.slf4j.Slf4JStopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Provider;

public class App implements Runnable {

	private static Logger logger = LoggerFactory.getLogger(App.class);

	private String[] names;

	public App(String[] names) {
		this.names = names;
	}

	private static class ReallyRunner implements Runnable {
		@Inject
		private Generate g;

		private String name;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		@Override
		public void run() {
			logger.info(String
					.format("START-STEP [%s] [%s]", name, getNowStr()));
			//
			final String swName = String.format("step-%s", name);
			StopWatch swName2 = new Slf4JStopWatch(logger);
			swName2.start(swName);
			//
			g.setName(name);
			try {
				g.call();
			} catch (Exception e) {
				logger.error("FAIL", e);
			}
			//
			swName2.stop(swName);
		}
	}

	@Inject
	private Provider<ReallyRunner> reallyRunnerProvider;

	private ExecutorService executorService = null;

	@Override
	public void run() {
		StopWatch swApp = new Slf4JStopWatch(logger);
		swApp.start("app-main");
		logger.info(String.format("START-APP [%s]", getNowStr()));
		//
		final Configuration config = AppConfig.load();
		//
		initExecutorService(config);
		//
		for (final String name : names) {
			ReallyRunner runner = reallyRunnerProvider.get();
			runner.setName(name);
			//
			executorService.execute(runner);
		}
		//
		try {
			await(config);
		} catch (Exception e) {
			logger.error("SOMETHING WRONG!!!", e);
		}
		//
		swApp.stop("app-main");
		logger.info(String.format("END-APP [%s]", getNowStr()));
	}

	private void await(final Configuration config) throws Exception {
		//
		final long timeout = config.getLong("app.runner.time-out.n", 1);
		final String timeoutUnitStr = config.getString(
				"app.runner.time-out.unit", "HOURS");
		final TimeUnit timeoutUnit = toTimeunit(timeoutUnitStr);
		//
		try {
			logger.info(String
					.format("all jobs submitted, wait for shutting-down... (timeout = %s %s)",
							timeout, timeoutUnitStr));
			executorService.shutdown();
			executorService.awaitTermination(timeout, timeoutUnit);
			//
			logger.warn("TIMED-OUT OR ALL-TERMINATED, FORCE SHUTDOWN.");
			executorService.shutdownNow();
		} catch (InterruptedException e) {
			logger.error("USER INTERRUPT.", e);
		}
	}

	private TimeUnit toTimeunit(String timeoutUnitStr) throws Exception {
		Class<?> klass = TimeUnit.class;
		final Field[] fields = klass.getDeclaredFields();
		for (Field field : fields) {
			if (StringUtils.equals(timeoutUnitStr, field.getName())) {
				return (TimeUnit) field.get(klass);
			}
		}
		//
		throw new Exception(String.format("FIELD NOT-FOUND [%s] ON [%s]",
				timeoutUnitStr, klass));
	}

	private void initExecutorService(final Configuration config) {
		final int n = config.getInteger("app.runner.fixed-threads", 1);
		this.executorService = Executors.newFixedThreadPool(n);
		logger.info(String.format("fixed-thread-pool executor = %s, %s", n,
				this.executorService));
	}

	private static String getNowStr() {
		Date now = new Date();
		DateFormat dateFmt = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		return dateFmt.format(now);
	}

	public static void main(String[] args) {
		if (args.length == 0) {
			logger.error("PLEASE SPECIFY ONE OR MORE NAME(S).");
			return;
		} else {
			App app = new App(args);
			Guicer.get().injectMembers(app);
			app.run();
		}
	}

}
