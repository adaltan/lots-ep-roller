package lots.ep_roller.tests;

import lots.ep_roller.db.DbcpPool;
import lots.ep_roller.injections.Guicer;

import org.apache.commons.lang.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;

public class TestAppDbcpPool implements Runnable {

	public static void main(String[] args) {
		TestAppDbcpPool app = new TestAppDbcpPool();
		Guicer.get().injectMembers(app);
		app.run();
	}

	//

	@Inject
	private DbcpPool dbPool;

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public void run() {
		try {
			logger.info(ObjectUtils.toString(dbPool.getConnection("default")));
		} catch (Exception e) {
			logger.error("GET-CONNECTION FAIL!", e);
		}
	}

}
