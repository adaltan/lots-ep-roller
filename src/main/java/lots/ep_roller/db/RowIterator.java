package lots.ep_roller.db;

import java.sql.ResultSet;
import java.util.Map;

import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.RowProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.AbstractIterator;

public class RowIterator extends AbstractIterator<Map<String, Object>> {

	private ResultSet rs;
	private RowProcessor rowProcessor;

	public RowIterator(ResultSet rs) {
		super();
		this.rs = rs;
		this.rowProcessor = new BasicRowProcessor();
	}

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	protected Map<String, Object> computeNext() {
		try {
			if (rs.next()) {
				return rowProcessor.toMap(rs);
			} else {
				return endOfData();
			}
		} catch (Exception exc) {
			logger.error("computeNext FAIL!", exc);
			return endOfData();
		}
	}
}
