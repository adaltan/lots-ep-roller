package lots.ep_roller.db;

import java.sql.Connection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import lots.ep_roller.config.AppConfig;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.pool.impl.GenericObjectPool;

import com.google.inject.Singleton;

@Singleton
public class DbcpPool {

	private Map<String, DataSource> dataSources = null;

	public Connection getConnection(final String name) throws Exception {
		//
		initOnce(name);
		//
		if (dataSources.containsKey(name)) {
			return dataSources.get(name).getConnection();
		} else {
			throw new Exception(String.format("NO SUCH NAMED DATASOURCE [%s]",
					name));
		}
	}

	private void initOnce(final String name) throws Exception {
		//
		if (dataSources == null) {
			dataSources = Collections
					.synchronizedMap(new HashMap<String, DataSource>());
		}
		//
		if (!dataSources.containsKey(name)) {
			final String KEY_DRIVER = String.format("app.db.settings.%s.driver", name);
			final String KEY_URL = String.format("app.db.settings.%s.url", name);
			final String KEY_USERNAME = String.format("app.db.settings.%s.username",
					name);
			final String KEY_PASSWORD = String.format("app.db.settings.%s.password",
					name);
			final String KEY_MIN_IDLE = String.format("app.db.settings.%s.minIdle", name);
			final String KEY_MAX_ACTIVE = String.format("app.db.settings.%s.maxActive", name);
			//
			final Configuration config = AppConfig.load();
			final String driverStr = config.getString(KEY_DRIVER);
			final String urlStr = config.getString(KEY_URL);
			final String usernameStr = config.getString(KEY_USERNAME);
			final String passwordStr = config.getString(KEY_PASSWORD);
			final Integer minIdleInt = config.getInteger(KEY_MIN_IDLE, 3);
			final Integer maxActiveInt = config.getInteger(KEY_MAX_ACTIVE, 30);
			//
			Class<?> klass = Class.forName(driverStr);
			GenericObjectPool connectionPool = new GenericObjectPool();
			connectionPool.setMinIdle(minIdleInt);
			connectionPool.setMaxActive(maxActiveInt);
			ConnectionFactory connFactory = new DriverManagerConnectionFactory(
					urlStr, usernameStr, passwordStr);
			PoolableConnectionFactory factory = new PoolableConnectionFactory(
					connFactory, connectionPool, null, null, false, true);
			PoolingDataSource dataSource = new PoolingDataSource(
					factory.getPool());
			dataSources.put(name, dataSource);
		}
	}
}
