package lots.ep_roller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.Callable;

import lots.ep_roller.config.AppConfig;
import lots.ep_roller.db.DbcpPool;
import lots.ep_roller.db.RowIterator;
import lots.ep_roller.velocity.VelocityExpander;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ObjectUtils;
import org.perf4j.StopWatch;
import org.perf4j.slf4j.Slf4JStopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;

public class Generate implements Callable<Boolean> {

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Inject
	private DbcpPool dbPool;

	@Inject
	private VelocityExpander expander;

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public Boolean call() throws Exception {
		//
		final Configuration config = AppConfig.load();
		// DB Connection 얻기.
		Connection dbConn = dbPool.getConnection(getDbSettingName(config));
		// SQL 실행.
		final String sql = queryListing(config, name);
		PreparedStatement pstmt = dbConn.prepareStatement(sql);
		ResultSet rs = pstmt.executeQuery();
		// counting?
		final String swNameCounting = String.format("step-%s-sql-counting",
				getName());
		StopWatch swSqlCounting = new Slf4JStopWatch(logger);
		swSqlCounting.start(swNameCounting);
		final long count = countRows(config, dbConn, name);
		swSqlCounting.stop(swNameCounting);
		logger.info(String.format("%s count=%s", name, count));
		// 템플릿 확장 준비.
		final String swNameExpand = String.format("step-%s-velocity-expand",
				getName());
		StopWatch swVelocityExpand = new Slf4JStopWatch(logger);
		swVelocityExpand.start(swNameExpand);
		final Map<String, Object> ctx = new ImmutableMap.Builder<String, Object>()
				.put("resultSet", new RowIterator(rs))
				.put("newline", getNewlineStr(config))
				.put("resultSetLength", count).build();
		// 임시파일.
		final File tempFile = getTempFile();
		final String outputEncoding = config.getString(
				"app.velocity-output-encoding", "EUC-KR");
		FileOutputStream fileOutStream = new FileOutputStream(tempFile);
		FileChannel fileOutChannel = fileOutStream.getChannel();
		Writer writer = Channels.newWriter(fileOutChannel, outputEncoding);
		// 템플릿 확장해서 결과 텍스트 만들기, 다 만들었으면 임시파일->결과파일로 복사 후 임시파일 정리.
		expandTemplate(config, ctx, writer);
		// justShowResultSet(rs);
		// 파일 스트림 정리
		writer.close();
		fileOutChannel.close();
		fileOutStream.close();
		// 파일 복사 & 임시 파일 정리.
		final String resultPath = makeResultPath(config);
		copyResultAndCleanUpTempFile(resultPath, tempFile);
		swVelocityExpand.stop(swNameExpand);
		//
		rs.close();
		pstmt.close();
		dbConn.close();
		//
		return true;
	}

	private String getDbSettingName(final Configuration config) {
		return config.getString("app.db.default");
	}

	private String queryListing(final Configuration config, final String name)
			throws IOException {
		final String listingQueryId = config.getString(String.format(
				"app.rollers.%s.listing-query-name", name));
		// 실행할 문장.
		final String sql = getQueryString(config, listingQueryId);
		return sql;
	}

	private Object getNewlineStr(final Configuration config) throws Exception {
		final String dosOrUnix = config.getString("app.newline", "dos");
		if ("dos".equalsIgnoreCase(dosOrUnix)) {
			return "\r\n";
		} else if ("unix".equalsIgnoreCase(dosOrUnix)) {
			return "\n";
		} else {
			throw new Exception(String.format("WT??? [%s]", dosOrUnix));
		}
	}

	private void justShowResultSet(ResultSet rs) {
		for (Iterator<Map<String, Object>> iter = new RowIterator(rs); iter
				.hasNext();) {
			Map<String, Object> item = iter.next();
			System.out.println(item);
		}
	}

	private long countRows(final Configuration config, Connection dbConn,
			final String name) throws SQLException, Exception {
		final String countingQueryId = config.getString(String.format(
				"app.rollers.%s.counting-query-name", name));
		// 실행할 문장.
		final String sql = getQueryString(config, countingQueryId);
		PreparedStatement pstmt = dbConn.prepareStatement(sql);
		long count = 0L;
		ResultSet rsCount = pstmt.executeQuery();
		while (rsCount.next()) {
			count = rsCount.getLong(1);
		}
		rsCount.close();
		pstmt.close();
		return count;
	}

	private void expandTemplate(final Configuration config,
			final Map<String, Object> ctx, Writer writer) throws IOException {
		final String velocityBaseDir = config
				.getString("app.velocity-base-dir");
		final String templateFilename = config.getString(String.format(
				"app.rollers.%s.template-filename", name));
		expander.expand(velocityBaseDir, templateFilename, ctx, writer);
	}

	private String makeResultPath(final Configuration config) {
		final String outputDir = config.getString("app.result-base-dir");
		final String resultFilename = config.getString(String.format(
				"app.rollers.%s.result-filename", name));
		final String resultPath = outputDir + resultFilename;
		return resultPath;
	}

	private File getTempFile() throws IOException {
		final File tempFile = File.createTempFile(
				String.format("ep_roller_tmp-%s", name), ".txt");
		logger.info(String.format("temp-file = %s", tempFile));
		return tempFile;
	}

	/** 임시파일을 결과파일로 복사하고, 임시파일은 지우기. */
	private void copyResultAndCleanUpTempFile(final String resultPath,
			final File tempFile) {
		logger.info(String.format("copy [%s] -> [%s]",
				ObjectUtils.toString(tempFile),
				ObjectUtils.toString(resultPath)));
		try {
			FileUtils.copyFile(tempFile, new File(resultPath));
		} catch (Exception exc) {
			logger.warn(String.format("FAIL TO COPY [%s] -> [%s]", tempFile,
					resultPath), exc);
		}
		logger.info(String.format("delete temp-file [%s] => [%s]", tempFile,
				FileUtils.deleteQuietly(tempFile)));
	}

	/** 쿼리 파일 내용 읽어오기. (클래스로더 활용) */
	private String getQueryString(final Configuration config,
			final String queryId) throws IOException {
		final String baseDir = config.getString("app.sql-base-dir");
		final String inputEncoding = config.getString("app.sql-input-encoding",
				"UTF-8");
		final String path = baseDir + queryId;
		InputStream in = this.getClass().getResourceAsStream(path);
		if (in == null) {
			throw new IOException(String.format("%s not found!", path));
		}
		final String sql = IOUtils.toString(in, inputEncoding);
		return sql;
	}

}
