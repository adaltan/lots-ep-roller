package lots.ep_roller.velocity;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;
import java.util.Properties;

import lots.ep_roller.config.AppConfig;

import org.apache.commons.configuration.Configuration;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.context.Context;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.RuntimeServices;
import org.apache.velocity.runtime.log.LogChute;
import org.apache.velocity.tools.ToolManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VelocityExpander implements LogChute {

	private Logger logger = LoggerFactory.getLogger(getClass());

	public void expand(final String baseDir, final String filename,
			final Map<String, Object> ctx, Writer writer) throws IOException {
		VelocityEngine ve = new VelocityEngine();
		ve.setProperty(RuntimeConstants.RUNTIME_LOG_LOGSYSTEM_CLASS, this);
		//
		Configuration config = AppConfig.load();
		Properties p = new Properties();
		p.setProperty("input.encoding",
				config.getString("app.velocity-input-encoding", "UTF-8"));
		p.setProperty("output.encoding",
				config.getString("app.velocity-output-encoding", "EUC-KR"));
		p.setProperty("resource.loader", "class");
		p.setProperty("class.resource.loader.description",
				"Velocity Classpath Resource Loader");
		p.setProperty("class.resource.loader.class",
				"org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
		/*
		 * p.setProperty("tools.toolbox", "application");
		 * p.setProperty("tools.application.date",
		 * "org.apache.velocity.tools.generic.DateTool");
		 */
		// p.setProperty("org.apache.velocity.toolbox",
		// "/velocity-toolbox.xml");
		ToolManager manager = new ToolManager();
		// FIXME: manager.configure("/velocity-toolbox.xml");
		//
		ve.init(p);
		//
		Template t = ve.getTemplate(baseDir + filename);
		//
		Context context = manager.createContext();
		for (String k : ctx.keySet()) {
			context.put(k, ctx.get(k));
		}
		//
		t.merge(context, writer);
		writer.flush();
	}

	@Override
	public void init(RuntimeServices rs) throws Exception {
		// do nothing
	}

	@Override
	public void log(int level, String message) {
		switch (level) {
		case LogChute.DEBUG_ID:
			logger.debug(message);
			break;
		case LogChute.ERROR_ID:
			logger.error(message);
			break;
		case LogChute.INFO_ID:
			logger.info(message);
			break;
		case LogChute.TRACE_ID:
			logger.trace(message);
			break;
		case LogChute.WARN_ID:
			logger.warn(message);
			break;
		default:
			break;
		}
	}

	@Override
	public void log(int level, String message, Throwable t) {
		switch (level) {
		case LogChute.DEBUG_ID:
			logger.debug(message, t);
			break;
		case LogChute.ERROR_ID:
			logger.error(message, t);
			break;
		case LogChute.INFO_ID:
			logger.info(message, t);
			break;
		case LogChute.TRACE_ID:
			logger.trace(message, t);
			break;
		case LogChute.WARN_ID:
			logger.warn(message, t);
			break;
		default:
			break;
		}
	}

	@Override
	public boolean isLevelEnabled(int level) {
		return true;
	}
}
