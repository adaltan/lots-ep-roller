# EP Roller

	- 2012/12/21, 윤종혁
	
	제휴 상품 정보고시를 위한 정보 텍스트 파일을 생성.
	
	
## 프로젝트 세팅 / 빌드 방법

	1. Maven 2+으로 설정. (이클립스에서 Juno이상이거나 m2eclipse을 설치한 상태에서, Import / Existing Maven Project)
	
	2. mvn/install-file-to-local-mvn-artifacts.cmd 배치파일 실행하여 Oracle JDBC Driver등 non-public Maven Artifact들을 개발자 PC의 로컬 캐쉬에 저장해야함.
	
	3. "mvn package" 실행하여, 의존성 포함한 실행가능한 JAR(Executable JAR)을 생성 가능.
	
	4. target/ep-roller-0.0.1-SNAPSHOT-jar-with-dependencies.jar 와 같이 결과 JAR 파일이 생성됨.

	

## 실행 방법

	1. 다음과 같이 실행하면 됩니다. (위에서 설명한 결과 JAR 파일으로)
		> java -jar ep-roller.jar newadd_naver
			- 위의 예시에서는 newadd_naver을 실행하여 EP결과 파일을 생성한 예시입니다.
			
		> java -jar ep-roller.jar update_naver update_basket
			- 위와 같이 둘 이상의 대상을 지정하여, update_naver, update_basket 모두 생성할 수 있습니다.


## 주요 설정 파일


	* DB 접속
		- src/main/resources/app-config/02-db.xml
	
	
	* SQL 쿼리
		- src/main/resources/lots/ep_roller/queries 디렉토리.
		- 여기에 있는 파일이름으로 이후 롤러 설정에 지정함.
	
	
	* EP전문 템플릿
		- src/main/resources/lots/ep_roller/result_templates 디렉토리.
		- 여기에 있는 파일이름으로 이후 롤러 설정에서 지정함.
		- Apache Velocity 문법을 활용하므로 반드시 Apache Velocity / User Guide 참고할것.
		- resultSet, resultSetLength만을 미리 설정해준다.
			- resultSet : iterator이며, foreach으로 접근가능하며, SQL결과의 매 Row을 Map<String, Object>으로 접근 가능. (컬럼이름, 값)
			- resultSetLength : 전체 SQL 실행의 결과 카운트 정수값(long).
	
	
	* 디렉토리, 파일 인코딩 등
		- src/main/resources/app-config/01-dirs.xml
			- EP전문 템플릿 파일들의 디렉토리. (classloader 기준)
			- EP전문 결과 파일 생성 디렉토리. (파일시스템 절대 경로)
			- EP전문 템플릿 파일의 입력 인코딩. (UTF-8, EUC-KR)
			- EP전문 결과 파일의 출력 인코딩. (UTF-8, EUC-KR)
			- SQL 파일의 디렉토리. (classloader 기준)
			- SQL 파일의 입력 인코등. (UTF-8, EUC-KR)
	
	
	* SQL 쿼리 + EP전문 템플릿
		- src/main/resources/app-config/rollers.xml
		- SQL쿼리와 EP전문 템플릿을 합쳐서  결과 전문을 생성하는 설정.
		- 추가적으로 결과 파일의 이름을 지정.
		- 예시:
			<config>
				<app>	
					<rollers>					
						<!-- SK바스켓 (기본) -->
						<basket>
							<result-filename>basket.txt</result-filename>
							<query-name>listBasicEpProds.sql</query-name>
							<template-filename>basket.velocity.txt</template-filename>
						</basket>
					</rollers>
				</app>
			</config>
			
			- 위와 같이 설정하면, 실행시 프로그램의 인자로 "basket"을 지정하여 생성.
			- basket.txt을 결과 파일로 생성. (결과 파일 디렉토리에 위치.)
			- listBasicEpPRods.sql 파일을 SQL으로 실행한 결과로 결과 파일 생성. (SQL 파일 디렉토리에 위치한.)
			- basket.velocity.txt 파일을 템플릿으로 사용하여 결과 파일 생성. (템플릿 디렉토리에 위치한.)
		
	
	* 스레딩 처리
		- src/main/resources/app-config/03-threading.xml
			- 최대 실행시간을 지정 가능. 
				- app.runner.time-out.n 와 app.runner.time-out.unit 으로 지정.
				- 참고: http://docs.oracle.com/javase/6/docs/api/java/util/concurrent/ExecutorService.html#awaitTermination(long, java.util.concurrent.TimeUnit)
	
		- 몇개의 스레드로 동시 실행할지 지정 가능.
			- 고정 크기의 스레드만 지정 가능.
			- fixed-threads 으로 지정.
			- 참고: http://docs.oracle.com/javase/6/docs/api/java/util/concurrent/Executors.html#newFixedThreadPool(int)

	
###끝